###############################################################################
# Librerama                                                                   #
# Copyright (C) 2023 Michael Alexsander                                       #
#-----------------------------------------------------------------------------#
# This file is part of Librerama.                                             #
#                                                                             #
# Librerama is free software: you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# Librerama is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with Librerama.  If not, see <http://www.gnu.org/licenses/>.          #
###############################################################################

extends Node


signal control_type_changed

enum ControlTypes {
	AUTOMATIC,
	TOUCH,
	JOYPAD,
	JOYPAD_TOUCH,
	KEYBOARD,
}

const _NANOGAME_INPUT_ACTIONS = [
	"nanogame_up",
	"nanogame_down",
	"nanogame_left",
	"nanogame_right",
	"nanogame_action"
]

var main_control: int = ControlTypes.AUTOMATIC setget set_main_control


func set_main_control(control: int) -> void:
	if control < 0 or control >= ControlTypes.size():
		push_error('Invalid control type of index "' + str(control) + '".')

	if control == main_control:
		return

	var control_type = get_control_type()

	main_control = control

	if control_type != get_control_type():
		emit_signal("control_type_changed")


func is_using_joypad() -> bool:
	var control_type: int = get_control_type()
	return control_type == ControlTypes.JOYPAD or\
			control_type == ControlTypes.JOYPAD_TOUCH


func get_nanogame_input_actions() -> Array:
	return _NANOGAME_INPUT_ACTIONS.duplicate()


func get_control_type() -> int:
	if main_control != ControlTypes.AUTOMATIC:
		return main_control

	if Input.get_connected_joypads().empty() and OS.has_touchscreen_ui_hint():
		return ControlTypes.TOUCH

	if not Input.get_connected_joypads().empty():
		return ControlTypes.JOYPAD_TOUCH if OS.has_touchscreen_ui_hint()\
				else ControlTypes.JOYPAD

	return ControlTypes.KEYBOARD
